# Experiment

## Before you start:

1. Clone the repository. And read this readme.
2. Do NOT read the code or the questionaire yet
3. Make sure running the tests with `gradlew test` compiles and runs the tests (or you have another way to run the tests).
   Please see section [gradle commands](#gradle-commands) below for further instructions.
4. Make sure you have about 2 hours of free time without other distractions.
5. Open <https://st116290.bitbucket.io> and enable notifications for the site (the site only saves data in your browser)
6. Test the message sending (big textbox below) and task start stop buttons (left side). Make sure that notifications work (should come only when task has them enabled after max 2 minutes)
7. Reset all message and task data after playing around with the site! (the two buttons in the lower left corner)

## During the experiment

* Make sure that you will not be distracted (e.g. by other messenger notifications) during the experiment
* Do the tasks on the left from top to bottom.
* Make sure tu start and end the task on the website.
* You can open the task class before starting the task on the webite.
* The tasks have a time limit and you will get a notification once the limit is reached. You should stop working on the task after the time limit is reached
* Make sure to have a short pause between the tasks to refresh your concentration
* You can use your favourite editor and debugger to complete the tasks

### Algorithm tasks

You will find a pseudocode sorting algorithm inside a comment in the class.
Your task is to implement  this algorithm in java.
Do NOT use copy/paste of the pseudocode to speed up your programming.

### Bug finding tasks

You will find a method in the class that implements some algorithm.
The specification of the algorithm is in the doc comment above the method.
Your task is to find the Bugs in the implementation and to fix them (if possible).
If all bugs are fixed then all tests for the class will pass.
Each bug causes at least one test to fail.
If you have found a bug but don't know how to fix it mark the bug in the sourcecode with a comment describing the bug.

## After the experiment

1. Fill in the questionaire.pdf in this repository.
2. Export the data of the website with the export button (bottom left corner) and save the json(s) into the file `messenger-data.txt`.
3. Create a zip containing the questionaire, messenger-data.pdf and the whole src folder.
4. Send the zip per mail to st116290@stud.uni-stuttgart.de with the subject `FMST experiment group A`


## gradle commands

Setup:

You need a gradle installation on your computer [https://gradle.org/install/](https://gradle.org/install/).

To initialize the wrapper run the following command inside your shell.

```bash
gradle wrapper
```

To run all tests:

```bash
./gradlew test
```

To run a test for a specific class:

```bash
./gradlew bugA
./gradlew sortA
```
