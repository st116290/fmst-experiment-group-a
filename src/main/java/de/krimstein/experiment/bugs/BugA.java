package de.krimstein.experiment.bugs;

public final class BugA {

    /**
     * Find the first occurence of the largest Sequence of the same character in
     * a {@link String} of '0' and '1' characters.
     *
     * @param input example: "0110001111"
     * @return
     */
    public static Result findLargestContinousString(String input) {
        int longestStartPos = 0;
        int longestSequenceLength = 0;
        int currentStartPos = 0;
        int currentSequenceLength = 0;

        char lastChar = '0';
        int i = 0;
        while (i <= input.length()) {
            if (input.charAt(i) != lastChar) {
                if (currentSequenceLength >= longestSequenceLength) {
                    longestSequenceLength = currentSequenceLength;
                    longestStartPos = currentStartPos;
                }
                lastChar = input.charAt(i);
                currentStartPos = ++i;
                currentSequenceLength = 1;
                continue;
            }
            currentSequenceLength++;
            i++;
        }
        return new Result(longestSequenceLength, longestStartPos);
    }

    /**
     * Helper class for storing algorithm results.
     */
    public static final class Result {
        public final int length;
        public final int position;

        public Result(int length, int position) {
            this.length = length;
            this.position = position;
        }
    }
}
