package de.krimstein.experiment.bugs;

import java.util.List;
import java.util.LinkedList;

public final class BugB {

    /**
     * Parse a ';'-seperated {@link String} into a Collection of Strings.
     *
     * If the parser encounters the String "END" he will break the parsing and
     * return the result.
     *
     * The parser should not put empty Strings in the result.
     *
     * The parser treats the end of the string as an implicit ';'.
     *
     * @param input example: "Hello;world;END;"
     * @return
     */
    public static List<String> parse(String input) {
        List<String> result = new LinkedList<>();
        int currentSart = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == ';') {
                String currentString = input.substring(currentSart, i);
                if (currentString == "END") {
                    continue;
                }
                if (currentString.length() > 1) {
                    result.add(currentString);
                }
                currentSart = ++i;
            }
        }
        return result;
    }
}
