package de.krimstein.experiment.bugs;

public final class BugC {

    /**
     * Parse a string literal with the following properties:
     *
     * Strings can be enclosed in ' or ".
     *
     * '"' and "'" are valid strings.
     *
     * "\" can escape the following characters:
     *
     *  \" = "
     *  \' = '
     *  \\ = \
     *  \n = [newline character]
     *
     * String literals concatenate automatically:
     *
     * 'Hello ' "world" = "Hello world"
     *
     * Any non whitespace character outside of quotes leads to an error.
     *
     * @param input example: "Hello \nworld!"
     * @return
     */
    public static String parse(String input) {
        String result = "";
        char currentQuote = '"';
        boolean insideString = false;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (!insideString) {
                if (c == '"' || c == '\'') {
                    currentQuote = '"';
                    insideString = true;
                } else if ((c != ' ') && (c != '\n')) {
                    throw new IllegalArgumentException("Malformed String literal.");
                }
            }
            if (insideString) {
                if (c == '"' || c == '\'') {
                    if (c == currentQuote) {
                        insideString = false;
                        continue;
                    }
                } else if (c == '\\') {
                    if (i+1 >= input.length()) {
                        throw new IllegalArgumentException("Incomplete Escape Sequence.");
                    }
                    char next = input.charAt(i+1);
                    switch (next) {
                        case '\'':
                        case '\"':
                        case '\\':
                            result += next;
                        case 't': result += '\t'; break;
                        case 'n': result += '\n'; break;
                        default: throw new IllegalArgumentException("Unknown Escape Sequence.");
                    }
                    continue;
                }
                result += c;
            }
        }
        if (insideString) {
            throw new IllegalArgumentException("Missing end quotes.");
        }
        return result;
    }
}
