package de.krimstein.experiment.implementation;

import java.util.List;

/**
 * The generic interface to represent a sort algorithm. Actual {@link SortAlgorithm}s should
 * implement this interface, for an implementation independent testing.
 *
 * @param <T> A generic parameter to indicate, that the sort algorithm implementing the interface
 * could sort different element types.
 */
public interface SortAlgorithm<T> {

  /**
   * Interface function representing a sort algorithm, which takes a shuffled {@link List} as input,
   * and sorts the elements in the {@link List} in ascending order. The sorted list is returned
   * after sorting.
   *
   * @param list The shuffled {@link List} to be sorted.
   * @return The {@link List} sorted in an ascending order.
   */
  List<T> sort(final List<T> list);
}
