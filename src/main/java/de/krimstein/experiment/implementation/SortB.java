package de.krimstein.experiment.implementation;

import java.util.List;

public final class SortB implements SortAlgorithm<Integer> {

    /*
        for (i in [0, list.length[) {
            var tmp = list[i]
            var j = i-1
            while (j>=0 AND list[j] > tmp)
                list[j+1] = list[j]
                j--
            }
            list[j+1] = tmp
        }
        return list
     */


    @Override
    public List<Integer> sort(final List<Integer> list) {
        // Implement sorting algorithm here
        return list;
    }
}