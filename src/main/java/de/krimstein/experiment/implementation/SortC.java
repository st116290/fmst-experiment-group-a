package de.krimstein.experiment.implementation;

import java.util.ArrayList;
import java.util.List;

public final class SortC implements SortAlgorithm<Integer> {

    /*
        function Integer[] sort(Integer[] list) {
            if (list <= 1) {
                return list
            }
            var pivot = choosePivot(list)
            var lessThenPivot = new Integer[]
            var greaterOrEqualToPivot = new Integer[]
            for (element in list) {
                if (element < pivot) {
                    lessThenPivot.add(element)
                } else {
                    greaterOrEqualToPivot.add(element)
                }
            }
            return sort(lessThanPivot) + sort(greaterOrEqualToPivot)
        }
     */


    @Override
    public List<Integer> sort(final List<Integer> list) {
        // Implement sorting algorithm here
        return list;
    }

    private Integer choosePivot(List<Integer> list) {
        return list.get((int) Math.floor(Math.random()*list.size()));
    }
}
