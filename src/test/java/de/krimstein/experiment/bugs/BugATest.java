package de.krimstein.experiment.bugs;

import org.junit.Assert;
import org.junit.Test;

public final class BugATest {

    @Test
    public void testEmpty() {
        String input = "";
        System.out.println("Testing with \"" + input + "\"");
        BugA.Result result = BugA.findLargestContinousString(input);
        System.out.println("length: " + result.length + ", position: " + result.position);
        Assert.assertEquals("Wrong sequence length.", 0, result.length);
        Assert.assertEquals("Wrong sequence start.", 0, result.position);
    }

    @Test
    public void testAllZero() {
        String input = "000";
        System.out.println("Testing with \"" + input + "\"");
        BugA.Result result = BugA.findLargestContinousString(input);
        System.out.println("length: " + result.length + ", position: " + result.position);
        Assert.assertEquals("Wrong sequence length.", 3, result.length);
        Assert.assertEquals("Wrong sequence start.", 0, result.position);
    }

    @Test
    public void testAllOne() {
        String input = "1111";
        System.out.println("Testing with \"" + input + "\"");
        BugA.Result result = BugA.findLargestContinousString(input);
        System.out.println("length: " + result.length + ", position: " + result.position);
        Assert.assertEquals("Wrong sequence length.", 4, result.length);
        Assert.assertEquals("Wrong sequence start.", 0, result.position);
    }

    @Test
    public void testTwoEqualLength() {
        String input = "011101110";
        System.out.println("Testing with \"" + input + "\"");
        BugA.Result result = BugA.findLargestContinousString(input);
        System.out.println("length: " + result.length + ", position: " + result.position);
        Assert.assertEquals("Wrong sequence length.", 3, result.length);
        Assert.assertEquals("Wrong sequence start.", 1, result.position);
    }

    @Test
    public void testMulti() {
        String input = "0110111011110";
        System.out.println("Testing with \"" + input + "\"");
        BugA.Result result = BugA.findLargestContinousString(input);
        System.out.println("length: " + result.length + ", position: " + result.position);
        Assert.assertEquals("Wrong sequence length.", 4, result.length);
        Assert.assertEquals("Wrong sequence start.", 8, result.position);
    }
}
