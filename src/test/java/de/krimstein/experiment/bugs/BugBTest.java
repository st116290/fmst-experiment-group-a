package de.krimstein.experiment.bugs;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public final class BugBTest {

    @Test
    public void testEmpty() {
        String input = "";
        System.out.println("Testing with \"" + input + "\"");
        List<String> result = BugB.parse(input);
        System.out.println(result);
        Assert.assertEquals("Wrong list length.", 0, result.size());
    }

    @Test
    public void testOnlyDelimiter() {
        String input = ";;;";
        System.out.println("Testing with \"" + input + "\"");
        List<String> result = BugB.parse(input);
        System.out.println(result);
        Assert.assertEquals("Wrong list length.", 0, result.size());
    }

    @Test
    public void testEnd() {
        String input = "END;";
        System.out.println("Testing with \"" + input + "\"");
        List<String> result = BugB.parse(input);
        System.out.println(result);
        Assert.assertEquals("Wrong list length.", 0, result.size());
    }

    @Test
    public void testEndWithTrailingData() {
        String input = "END;Hello;";
        System.out.println("Testing with \"" + input + "\"");
        List<String> result = BugB.parse(input);
        System.out.println(result);
        Assert.assertEquals("Wrong list length.", 0, result.size());
    }

    @Test
    public void testSingleInput() {
        String input = "Hello;";
        System.out.println("Testing with \"" + input + "\"");
        List<String> result = BugB.parse(input);
        System.out.println(result);
        Assert.assertEquals("Wrong list length.", 1, result.size());
        Assert.assertEquals("Wrong String in position 0", "Hello", result.get(0));
    }

    @Test
    public void testNormalInput() {
        String input = "Hello;World;END;";
        System.out.println("Testing with \"" + input + "\"");
        List<String> result = BugB.parse(input);
        System.out.println(result);
        Assert.assertEquals("Wrong list length.", 2, result.size());
        Assert.assertEquals("Wrong String in position 0", "Hello", result.get(0));
        Assert.assertEquals("Wrong String in position 1", "World", result.get(1));
    }

    @Test
    public void testSingleInputWithoutDelimiter() {
        String input = "Hello";
        System.out.println("Testing with \"" + input + "\"");
        List<String> result = BugB.parse(input);
        System.out.println(result);
        Assert.assertEquals("Wrong list length.", 1, result.size());
        Assert.assertEquals("Wrong String in position 0", "Hello", result.get(0));
    }

}
