package de.krimstein.experiment.bugs;

import org.junit.Assert;
import org.junit.Test;

public final class BugCTest {

    @Test
    public void testEmpty() {
        String input = "";
        System.out.println("Testing with \"" + input + "\"");
        String result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Result was not empty String.", "", result);
    }

    @Test
    public void testEmptyLiteralSinglequote() {
        String input = "''";
        System.out.println("Testing with \"" + input + "\"");
        String result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Result was not empty String.", "", result);
    }

    @Test
    public void testEmptyLiteralDoublequote() {
        String input = "\"\"";
        System.out.println("Testing with \"" + input + "\"");
        String result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Result was not empty String.", "", result);
    }

    @Test
    public void testSinglequoteInDoublequote() {
        String input = "\"'\"";
        System.out.println("Testing with \"" + input + "\"");
        String result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Wrong result String.", "'", result);
    }

    @Test
    public void testDoublequoteInSinglequote() {
        String input = "'\"'";
        System.out.println("Testing with \"" + input + "\"");
        String result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Wrong result String.", "\"", result);
    }

    @Test
    public void testEscaping() {
        String input = "'Hello \\nworld!'";
        System.out.println("Testing with \"" + input + "\"");
        String result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "Hello \nworld!", result);
    }

    @Test
    public void testEscapeSequences() {
        String input = "'\\\"'";
        System.out.println("Testing with \"" + input + "\"");
        String result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "\"", result);

        input = "'\\''";
        System.out.println("Testing with \"" + input + "\"");
        result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "'", result);

        input = "'\\\\'";
        System.out.println("Testing with \"" + input + "\"");
        result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "\\", result);

        input = "'\\t'";
        System.out.println("Testing with \"" + input + "\"");
        result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "\t", result);

        input = "'\\n'";
        System.out.println("Testing with \"" + input + "\"");
        result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "\n", result);
    }

    @Test
    public void testNormalStrings() {
        String input = "'Hello'";
        System.out.println("Testing with \"" + input + "\"");
        String result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "Hello", result);

        input = "\"Hello\"";
        System.out.println("Testing with \"" + input + "\"");
        result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "Hello", result);
    }

    @Test
    public void testStringConcatenation() {
        String input = "'Hello ' 'world!'";
        System.out.println("Testing with \"" + input + "\"");
        String result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "Hello world!", result);

        input = "\"Hello \" 'world!'";
        System.out.println("Testing with \"" + input + "\"");
        result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "Hello world!", result);

        input = "'Hello ' \"world!\"";
        System.out.println("Testing with \"" + input + "\"");
        result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "Hello world!", result);

        input = "\"Hello \" \"world!\"";
        System.out.println("Testing with \"" + input + "\"");
        result = BugC.parse(input);
        System.out.println("Result: " + result);
        Assert.assertEquals("Escape Sequence not working.", "Hello world!", result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncompleteLiteral() {
        String input = "'Hello world";
        System.out.println("Testing with \"" + input + "\"");
        BugC.parse(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMalformedLiteral() {
        String input = "'Hello ' world '!'";
        System.out.println("Testing with \"" + input + "\"");
        BugC.parse(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnknownEscapeSequence() {
        String input = "'Hello \\o world!'";
        System.out.println("Testing with \"" + input + "\"");
        BugC.parse(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInkompleteEscapeSequence() {
        String input = "'Hello \\";
        System.out.println("Testing with \"" + input + "\"");
        BugC.parse(input);
    }

}
