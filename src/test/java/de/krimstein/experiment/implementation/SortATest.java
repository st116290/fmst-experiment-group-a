package de.krimstein.experiment.implementation;

import org.junit.Test;

public final class SortATest extends SortAlgorithmTest {

  @Test
  public void testSortA() {
    final SortAlgorithm<Integer> sortA = new SortA();
    this.testSortAlgorithm(sortA);
  }
}
