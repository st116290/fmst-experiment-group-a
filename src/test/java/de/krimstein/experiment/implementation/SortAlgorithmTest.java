package de.krimstein.experiment.implementation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;

public class SortAlgorithmTest {

  private List<Integer> createExpected() {
    final int n = 10;
    final List<Integer> list = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      list.add(i);
    }
    return list;
  }

  void testSortAlgorithm(final SortAlgorithm<Integer> sortAlgorithm) {
    final List<Integer> expected = this.createExpected();
    final List<Integer> shuffled = this.createExpected();
    Collections.shuffle(shuffled);
    final List<Integer> reversed = this.createExpected();
    Collections.reverse(reversed);

    final List<Integer> actualShuffled = sortAlgorithm.sort(shuffled);
    Assert.assertEquals("The sort algorithm lost or added elements!", expected.size(), actualShuffled.size());
    for (int i = 0, n = actualShuffled.size(); i < n; i++) {
      Assert.assertEquals("The list was not sorted correctly", expected.get(i), actualShuffled.get(i));
    }

    final List<Integer> actualReversed = sortAlgorithm.sort(shuffled);
    Assert.assertEquals("The sort algorithm lost or added elements!", expected.size(), actualReversed.size());
    for (int i = 0, n = actualReversed.size(); i < n; i++) {
      Assert.assertEquals("The list was not sorted correctly", expected.get(i), actualReversed.get(i));
    }
  }
}
