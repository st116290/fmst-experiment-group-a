package de.krimstein.experiment.implementation;

import org.junit.Test;

public final class SortBTest extends SortAlgorithmTest {

  @Test
  public void testSortB() {
    final SortAlgorithm<Integer> sortB = new SortB();
    this.testSortAlgorithm(sortB);
  }
}
