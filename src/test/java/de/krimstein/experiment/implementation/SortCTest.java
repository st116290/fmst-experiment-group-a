package de.krimstein.experiment.implementation;

import org.junit.Test;

public final class SortCTest extends SortAlgorithmTest {

  @Test
  public void testSortV() {
    final SortAlgorithm<Integer> sortC = new SortC();
    this.testSortAlgorithm(sortC);
  }
}
